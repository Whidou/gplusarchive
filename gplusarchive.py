#!/usr/bin/python

# GPlusArchive: A Google+ static archive generator
#
# Author: Whidou <root@whidou.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from hashlib import md5
import jinja2 as jinja
import json
import os
from os.path import join
import sys



def fetch_image_name(url, folder=None):
    if not folder:
        return ""

    code = md5(url.encode('utf-8')).hexdigest()
    for image in os.listdir(folder):
        if len(image) < 36:
            continue
        if image[:32] == code:
            return join(folder, image)
    return ""


def output_post(post, parent, env, output="output"):
    template = env.get_template("post.html")
    outFile = open(join(output, "{}.html".format(post["id"])), "w")
    outFile.write(template.render(post=post, parent=parent))
    outFile.close()


def output_collection(collection, env, output="output"):
    print("Exporting collection", collection["name"])

    template = env.get_template("collection.html")
    outFile = open(join(output, "{}.html".format(collection["id"])), "w")
    outFile.write(template.render(collection=collection))
    outFile.close()

    for post in collection["posts"]:
        output_post(post, collection, env, output)


def output_community(community, env, output="output"):
    print("Exporting community", community["name"])

    if "posts" not in community:
        community["posts"] = []

    for category in community["categories"]:
        for post in category["posts"]:
            post["category"] = category
            community["posts"].append(post)
    community["posts"].sort(key=lambda p: p["createdAt"], reverse=True)

    template = env.get_template("community.html")
    outFile = open(join(output, "{}.html".format(community["id"])), "w")
    outFile.write(template.render(community=community))
    outFile.close()

    for post in community["posts"]:
        output_post(post, community, env, output)


def output_account(account, env, output="output"):
    print("Exporting account", account["name"])

    template = env.get_template("account.html")
    outFile = open(join(output, "{}.html".format(account["id"])), "w")
    outFile.write(template.render(account=account))
    outFile.close()

    for post in account["posts"]:
        output_post(post, account, env, output)
    for collection in account["collections"]:
        output_collection(collection, env, output)
    for community in account["communities"]:
        output_community(community, env, output)


def output_index(root, env, output="output"):
    root["communities"] = []
    root["collections"] = []
    for account in root["accounts"]:
        output_account(account, env, output)
        root["communities"] += account["communities"]
        root["collections"] += account["collections"]

    print("Exporting index")

    root["communities"].sort(key=lambda c: c["name"])
    root["collections"].sort(key=lambda c: c["name"])

    template = env.get_template("index.html")
    outFile = open(join(output, "index.html"), "w")
    outFile.write(template.render(accounts=root["accounts"],
                                  communities=root["communities"],
                                  collections=root["collections"]))
    outFile.close()


def generate_archive(root, images=None, output="output"):
    os.makedirs(output, exist_ok=True)

    env = jinja.Environment(loader=jinja.PackageLoader("gplusarchive"),
                            trim_blocks=True,
                            lstrip_blocks=True)
    env.filters["imgPath"] = lambda x: fetch_image_name(x, images)
    env.filters["userContent"] = lambda x: "googleusercontent.com" in x

    output_index(root, env, output)



if __name__ == "__main__":
    from argparse import ArgumentParser as ap, FileType

    parser = ap(description="A Google+ static archive generator.")
    parser.add_argument("json_file", metavar="JSON_FILE",
                        type=FileType("r"),
                        help="path of the exported JSON file")
    parser.add_argument("-i", "--images", metavar="IMAGE_FOLDER",
                        help="path to the exported image folder")
    parser.add_argument("-o", "--output", default="output",
                        help="output directory")

    args = parser.parse_args()
    root = json.load(args.json_file)
    args.json_file.close()

    generate_archive(root, args.images, args.output)

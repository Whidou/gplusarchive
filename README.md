GPlusArchive
============

A Google+ static archive generator


Requirements
------------

* Python 3
* Jinja2


Usage
-----

```
./gplusarchive.py <PATH TO JSON FILE> [PATH TO IMAGES DIRECTORY]
````
